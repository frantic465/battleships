﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace BattleShips
{
    static class Program
    {
        public static MainWindow mainWindow;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Connection connection = new Connection("192.168.0.103", 22881);
            //connection.start();
            mainWindow = new MainWindow();
            try
            {
                Application.Run(mainWindow);
            }
            catch (Exception)
            {
            }
        }
    }
}
