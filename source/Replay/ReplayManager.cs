﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.Serialization.Formatters;
using System.IO;

namespace BattleShips
{
    public class ReplayManager
    {
        public const int BufferSize = 8 * 1024;
        private Connection _connection = null;
        private Mutex _mutex = new Mutex();
        private BinaryWriter _binaryWriter;

        public ReplayManager(Connection connection)
        {
            _connection = connection;
            _binaryWriter = new BinaryWriter(new MemoryStream());
            _connection.onDataRecieved += new Connection.DataRecievedHandler(_onDataRecievedHandler);
        }

        public void saveModelToFile(String fileName)
        {
            _mutex.WaitOne();
            _binaryWriter.Flush();
            Helpers.copyStream(_binaryWriter.BaseStream, (Stream)(File.OpenWrite("")));
            _mutex.ReleaseMutex();
        }

        public void saveModelToFile(Stream stream)
        {
            _mutex.WaitOne();
            _binaryWriter.Flush();
            BinaryReader binaryReader = new BinaryReader(_binaryWriter.BaseStream);
            binaryReader.BaseStream.Position = 0;
            binaryReader.BaseStream.CopyTo(stream);
            _mutex.ReleaseMutex();
        }

        private void _onDataRecievedHandler(byte[] data)
        {
            _mutex.WaitOne();
            _binaryWriter.Write(data);
            _mutex.ReleaseMutex();
        }
    }
}
