﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX;

namespace BattleShips
{
    public class ShipModelApproximatedState : ApproximatedState
    {
        public ShipInfo info;
        public int resources;
    }

    public class ShipModel : ApproximatedModelBase<ShipInfo, ShipModelApproximatedState>
    {
        public ShipModel(int id)
            : base(id)
        {

        }

        public static float directionToAngle(ShipInfo.ShipDirection direction)
        {
            return ((float)direction * (float)Math.PI / -4.0f);
        }

        protected override void fillApproximatedState(int infoIndexFirst, int infoIndexSecond, float relativeTime)
        {
            _approximatedState.position = Helpers.linear(new Vector2(_infoHistory[infoIndexFirst].Value.posX, _infoHistory[infoIndexFirst].Value.posY), new Vector2(_infoHistory[infoIndexSecond].Value.posX, _infoHistory[infoIndexSecond].Value.posY), relativeTime);
            _approximatedState.info = _infoHistory[infoIndexFirst].Value;
            _approximatedState.resources = (int)Helpers.linear(_infoHistory[infoIndexFirst].Value.resource, _infoHistory[infoIndexSecond].Value.resource, relativeTime);

            float firstAngle = directionToAngle(_infoHistory[infoIndexFirst].Value.direction);
            float secondAngle = directionToAngle(_infoHistory[infoIndexSecond].Value.direction);

            if (Math.Abs(firstAngle - secondAngle) > (float)Math.PI)
            {
                if (firstAngle < -(float)Math.PI)
                {
                    firstAngle = firstAngle + (float)Math.PI * 2.0f;
                }
                else
                {
                    secondAngle = secondAngle + (float)Math.PI * 2.0f;
                }
            }
            _approximatedState.angle = Helpers.linear(firstAngle, secondAngle, relativeTime * 10.0f);
        }
        
    }    
}
