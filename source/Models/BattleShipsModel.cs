﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX;
using System.Threading;

namespace BattleShips
{
    public class BattleShipsModel : BaseModel
    {
        public static int timeBetweenStates = 2900;

        private Connection _connection = null;
        private int _strategiesNum = 0;
        private MapDescr _mapDescr = null;
        private List<ShipModel> _ships = new List<ShipModel>();
        private List<ShotModel> _shots = new List<ShotModel>();
        private bool _inGame = false;
        private int _modelTime = 0;
        private int _recievedModelTime = 0;
        private Mutex _mutex = new Mutex();
        private bool _running = true;


        public int strategiesNum
        {
            get { return _strategiesNum; }
        }

        public MapDescr mapDescr
        {
            get { return _mapDescr; }
        }

        public IEnumerable<ShipModel> ships
        {
            get { return _ships.ToList(); }
        }

        public IEnumerable<ShipModel> getShipsByStrategy(int strategyId)
        {
            return _ships.Where(ship => (ship.getCurrentState() != null && ship.getCurrentState().info.strategyId == strategyId));
        }

        public IEnumerable<ShotModel> shots
        {
            get { return _shots.ToList(); }
        }

        public int modelTime
        {
            get { return _modelTime; }
        }

        public float modelProgress
        {
            get
            {
                float result = (float)modelTime / (Math.Max(1, _recievedModelTime));
                return Math.Max(Math.Min(result, 1.0f), 0.0f);
            }
        }

        public BattleShipsModel()
        {
            _connection = null;
        }

        public BattleShipsModel(Connection connection)
        {
            _connection = connection;

            _connection.onConnectioonStateChangedEvent += new Connection.ConnectionStateChangedHandler(_onConnectionStateChanged);
            _connection.onInitEvent += new Connection.InitHandler(_onInitEvent);
            _connection.onMapDescrEvent +=new Connection.MapDescrHandler(_onMapDescrHandler);
            _connection.onShipsUpdateEvent += new Connection.ShipsUpdateHandler(_onShipsUpdateHandler);
            _connection.onShotsUpdateEvent += new Connection.ShotsUpdateHandler(_onShotsUpdateHandler);
            _connection.onTermEvent += new Connection.TermHandler(_onTermHandler);
        }

        public void pause()
        {
            _running = false;
        }

        public void resume()
        {
            _running = true;
        }

        public bool isInGame
        {
            get { return _inGame; }
        }

        public void _onConnectionStateChanged(Connection.State state)
        {
            _mutex.WaitOne();
            _mutex.ReleaseMutex();            
        }
        
        private void _onInitEvent(int strategiesNum)
        {
            Log.info("Number of strategies changed to " + strategiesNum);
            _mutex.WaitOne();
            _strategiesNum = strategiesNum;
            _inGame = true;
            _mutex.ReleaseMutex();
        }

        private void _onTermHandler()
        {
            Log.info("Connection terminated");
            _mutex.WaitOne();
            _inGame = false;
            _mutex.ReleaseMutex();
        }

        private void _onMapDescrHandler(MapDescr mapDesc)
        {
            Log.info("Map descriptor changed to " + mapDesc);
            _mutex.WaitOne();
            _mapDescr = mapDesc;
            _mutex.ReleaseMutex();
        }

        private void _onShipsUpdateHandler(ShipInfo[] shipInfos)
        {
            _mutex.WaitOne();
            _recievedModelTime += timeBetweenStates;
            Log.info("Ships state changed to:");
            Log.info("id direction missiles repair resource shells speed strategyId torpedoes type    posX posY");
            foreach (ShipInfo shipInfo in shipInfos)
            {
                Log.info(shipInfo.id + "  " + shipInfo.direction + "      " + shipInfo.missiles + "      " + shipInfo.repair + "      " + shipInfo.resource
                         + "       " + shipInfo.shells + "      " + shipInfo.speed + "     " + shipInfo.strategyId + "          " + shipInfo.torpedoes
                         + "        " + shipInfo.type + " " + shipInfo.posX + "   " + shipInfo.posY);
                ShipModel model = _ships.Find(ship => ship.id == shipInfo.id);
                if (model == null)
                {
                    model = new ShipModel(shipInfo.id);
                    _ships.Add(model);
                }

                model.pushState(_recievedModelTime, shipInfo);
            }
            _mutex.ReleaseMutex();
        }

        private void _onShotsUpdateHandler(ShotInfo[] shotInfos)
        {
            _mutex.WaitOne();
            Log.info("Shots state changed to:");
            Log.info("id destinationX destinationY type posX posY");
            foreach (ShotInfo shotInfo in shotInfos)
            {
                Log.info(shotInfo.id + "  " + shotInfo.destinationX + "           " + shotInfo.destinationY
                         + "           " + shotInfo.type + "   " + shotInfo.posX + "    " + shotInfo.posY);
                ShotModel model = _shots.Find(shot => shot.id == shotInfo.id);
                if (model == null)
                {
                    model = new ShotModel(shotInfo.id);
                    _shots.Add(model);
                }

                model.pushState(_recievedModelTime, shotInfo);
            }
            _mutex.ReleaseMutex();
        }

        public void update(int elapsed) // elapsed in milliseconds
        {
            if (!_running)
            {
                return;
            }
            _mutex.WaitOne();
            _modelTime += elapsed;
            foreach (ShipModel ship in _ships)
            {
                ship.update(_modelTime);
            }

            foreach (ShotModel shot in _shots)
            {
                shot.update(_modelTime);
            }
            _mutex.ReleaseMutex();
        }

    }
}
