﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX;

namespace BattleShips
{
    public class ApproximatedState
    {
        public Vector2 position;
        public float angle; // in radians        
    };

    public abstract class ApproximatedModelBase<InfoType, ApproximatedStateType> 
        where InfoType : new() 
        where ApproximatedStateType : ApproximatedState, new()        
    {
        protected List<KeyValuePair<int, InfoType>> _infoHistory = new List<KeyValuePair<int, InfoType>>();
        protected ApproximatedStateType _approximatedState;
        public readonly int id;

        public ApproximatedModelBase(int id)
        {
            this.id = id;
        }

        public void pushState(int time, InfoType info)
        {
            _infoHistory.Add(new KeyValuePair<int, InfoType>(time, info));
        }

        public ApproximatedStateType getCurrentState()
        {
            return _approximatedState;
        }

        public void update(int currentTime)
        {
            //int stateIndex = currentTime / BattleShipsModel.timeBetweenStates;
            //int currentStateTime = currentTime % BattleShipsModel.timeBetweenStates;

            //if (_infoHistory.Count == 0)
            //{
            //    _approximatedState = null;
            //    return;
            //}

            //float currentStateTimeRelative = (float)currentStateTime / (float)BattleShipsModel.timeBetweenStates;

            //if (_approximatedState == null)
            //{
            //    _approximatedState = new ApproximatedState();

            //}
            //fillApproximatedState(currentStateTimeRelative);

            int infoIndexFirst = _infoHistory.FindLastIndex(el => el.Key < currentTime);
            int infoIndexSecond = _infoHistory.FindIndex(el => el.Key >= currentTime);
            if (infoIndexFirst == -1 || infoIndexSecond == -1 || infoIndexFirst >= infoIndexSecond)
            {
                _approximatedState = null;
                return;
            }
            if (_approximatedState == null)
            {
                _approximatedState = new ApproximatedStateType();
            }

            int firstTime = _infoHistory[infoIndexFirst].Key;
            int secondTime = _infoHistory[infoIndexSecond].Key;
            float relativeTime = (float)(currentTime - firstTime) / (secondTime - firstTime);

            fillApproximatedState(infoIndexFirst, infoIndexSecond, relativeTime);
        }

        protected abstract void fillApproximatedState(int infoIndexFirst, int infoIndexSecond, float relativeTime);
       
    }
}
