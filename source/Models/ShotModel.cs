﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX;

namespace BattleShips
{
    public class ShotModelApproximatedState : ApproximatedState
    {
        public ShotInfo info;
    }

    public class ShotModel : ApproximatedModelBase<ShotInfo, ShotModelApproximatedState>
    {
        public ShotModel(int id)
            : base(id)
        {

        }

        protected override void fillApproximatedState(int infoIndexFirst, int infoIndexSecond, float relativeTime)
        {
            _approximatedState.position = Helpers.linear(new Vector2(_infoHistory[infoIndexFirst].Value.posX, _infoHistory[infoIndexFirst].Value.posY), new Vector2(_infoHistory[infoIndexSecond].Value.posX, _infoHistory[infoIndexSecond].Value.posY), relativeTime);
            _approximatedState.info = _infoHistory[infoIndexFirst].Value;
            _approximatedState.angle = Helpers.angleBetweenVectors2(
                new Vector2(_infoHistory[infoIndexFirst].Value.destinationX - _infoHistory[infoIndexFirst].Value.posX, _infoHistory[infoIndexFirst].Value.destinationY - _infoHistory[infoIndexFirst].Value.posY),
                new Vector2(0.0f, 1.0f)
                );
        }

    }    
}
