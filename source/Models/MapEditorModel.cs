﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace BattleShips
{
    public class MapEditorModel : BaseModel
    {
        private MapDescr _mapDescr = null;
        private int _width = 0;
        private int _height = 0;
        private int _currentInstrument = -1;

        public int width
        {
            get { return _width; }
        }

        public int height
        {
            get { return _height; }
        }

        public MapDescr mapDescr
        {
            get { return _mapDescr; }            
        }

        public int currentInstrument
        {
            get { return _currentInstrument; }
        }

        public MapEditorModel()
        {

        }

        public void selectInstrument(int index)
        {
            _currentInstrument = index;
        }

        public void createNew(int width, int height)
        {
            _width = width;
            _height = height;
            _mapDescr = new MapDescr(_width, _height);
        }

        public void reset()
        {
            _width = _height = 0;
            _mapDescr = null;
        }

        public void saveToFile()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Battleships map|*.bsm";
            saveFileDialog.Title = "Save map to file";
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName == "")
            {
                return;
            }
            Stream fileStream = saveFileDialog.OpenFile();
            BinaryWriter binaryWriter = new BinaryWriter(fileStream);
            binaryWriter.Write(_width);
            binaryWriter.Write(_height);

            for (int y = 0; y < _height; ++y)
            {
                for (int x = 0; x < _width; ++x)
                {
                    binaryWriter.Write((byte)_mapDescr[x, y]);
                }
            }

                binaryWriter.Close();
            fileStream.Close();
        }

        public void loadFromFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Battleships map|*.bsm";
            openFileDialog.Title = "Save map to file";
            openFileDialog.ShowDialog();
            if (openFileDialog.FileName == "")
            {
                return;
            }
            Stream fileStream = openFileDialog.OpenFile();
            BinaryReader binaryReader = new BinaryReader(fileStream);

            try
            {
                _width = binaryReader.ReadInt32();
                _height = binaryReader.ReadInt32();

                byte[] data = binaryReader.ReadBytes(_width * _height);
                _mapDescr = new MapDescr(_width, _height, data);
            }
            catch (Exception)
            {
                String message = "Map file " + openFileDialog.FileName + " is corrupted.";
                Console.Out.WriteLine(message);
                Log.error(message);
                MessageBox.Show(message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            binaryReader.Close();
            fileStream.Close();            
        }
    }
}
