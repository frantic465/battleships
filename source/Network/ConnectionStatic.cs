﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShips
{
    public partial class Connection
    {
        static public int MAX_BUFFER_SIZE = 256;
        static public int INT_SIZE = 4;
        static public byte[] HELLO_MESSAGE = Encoding.ASCII.GetBytes("Hell2");
        static public byte[] MSG_INVALID_HEADER = Encoding.ASCII.GetBytes("Invalid data");
        static public byte[] MSG_INVALID_MAP_DESCR = Encoding.ASCII.GetBytes("Invalid map description");
        static public byte[] MSG_END_OF_GAME = Encoding.ASCII.GetBytes("The end of recieving data. Closing connection.");

        public delegate void ConnectionStateChangedHandler(Connection.State state);
        public delegate void InitHandler(int strategiesNum);
        public delegate void MapDescrHandler(MapDescr mapDescr);
        public delegate void ShipsUpdateHandler(ShipInfo[] ships);
        public delegate void ShotsUpdateHandler(ShotInfo[] shots);
        public delegate void TermHandler();
        public delegate void DataRecievedHandler(byte[] data);

        private class HeaderStruct
        {
            public int id;
            public int length;

            public HeaderStruct(int id, int length)
            {
                this.id = id;
                this.length = length;
            }
        }

        private enum Command
        {
            Ask = 10,
            EOF = -1,
            Init = 0,
            Map = 1,
            Ships = 2,
            Shots = 3
        }


    }
}
