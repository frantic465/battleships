﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;

namespace BattleShips
{
    
    public partial class Connection
    {
        public enum Mode
        {
            TCP_MODE,
            STREAM_MODE,
        };

        public enum State
        {
            NotConnected,
            Inited,
            Established,
        };

        private TcpClient _tcpClient = null;
        private Stream _stream = null;
        private string _hostname;
        private int _port;
        private Thread _recievingThread = null;
        private bool _shutdown = false;
        private State _state = State.NotConnected;
        private Mode _mode;

        public event ConnectionStateChangedHandler onConnectioonStateChangedEvent;
        public event InitHandler onInitEvent;
        public event MapDescrHandler onMapDescrEvent;
        public event ShipsUpdateHandler onShipsUpdateEvent;
        public event ShotsUpdateHandler onShotsUpdateEvent;
        public event TermHandler onTermEvent;
        public event DataRecievedHandler onDataRecieved;

        public Connection(string hostname, int port)
        {
            _hostname = hostname;
            _port = port;
            _mode = Mode.TCP_MODE;
        }

        public Connection(Stream inputStream)
        {
            _mode = Mode.STREAM_MODE;
            _stream = inputStream;
        }

        private void _changeState(State state)
        {
            if (_state != state)
            {
                _state = state;
                Log.info("Connection state changed to " + state);
                if (onConnectioonStateChangedEvent != null)
                {
                    onConnectioonStateChangedEvent(state);
                }
            }
        }

        public bool isConnectionEstablished
        {
            get { return _stream != null; }
        }

        public void start()
        {
            Debug.Assert(_recievingThread == null);
            _recievingThread = new Thread(new ThreadStart(_process));
            _recievingThread.IsBackground = true;
            _shutdown = false;
            _recievingThread.Start();
        }

        public void stop()
        {
            Debug.Assert(_recievingThread != null);
            _shutdown = true;
            _recievingThread = null;
        }

        private byte[] _read(int size)
        {
            int read = 0;
            byte[] buffer = new byte[size];
            while (read != size)
            {
                int bytesNum = _stream.Read(buffer, read, size - read);
                read += bytesNum;
                if (bytesNum <= 0)
                {
                    break;
                }
            }
            if (onDataRecieved != null)
            {
                onDataRecieved(buffer);
            }
            return buffer;
        }

        private int _readInt()
        {
            byte[] buffer = _read(INT_SIZE);
            if (buffer == null)
            {
                return 0;
            }
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(buffer);
            }
            return BitConverter.ToInt32(buffer, 0);
        }

        private HeaderStruct _readHeader()
        {
            byte[] buffer = _read(1);
            int id = (int)buffer[0];
            int len = _readInt();
            //Console.WriteLine("Header: id=" + id + "; len=" + len + ";");

            return new HeaderStruct(id, len);
        }

        private bool _openTcpConnection()
        {
            try
            {
                _tcpClient = new TcpClient(_hostname, _port);
                _tcpClient.ReceiveTimeout = 10000;
            }
            catch (Exception e)
            {
                _tcpClient = null;
                String message = "Can't connect to " + _hostname + ":" + _port;
                Console.WriteLine(message);
                Log.error(message);
                MessageBox.Show(message + "\n" + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            try 
            {
                _stream = _tcpClient.GetStream();
                _stream.Write(HELLO_MESSAGE, 0, HELLO_MESSAGE.Length);
            }            
            catch (Exception e)
            {
                _closeConnection();
                String message = "Can't connect to " + _hostname + ":" + _port;
                Log.error(message);
                MessageBox.Show(message + "\n" + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            Log.info("Opened connection to " + _hostname + ":" + _port);
            return true;
        }

        private void _closeConnection()
        {
            _changeState(State.NotConnected);
            if (onTermEvent != null)
            {
                onTermEvent();
            }
            String message = "Closing connection " + _hostname + ":" + _port;
            Console.Out.WriteLine(message);
            Log.info(message);
            if (_stream != null)
            {
                _stream.Close();
                _stream = null;
            }

            if (_tcpClient != null)
            {
                _tcpClient.Close();
                _tcpClient = null;
            }
        }

        private bool _handleInit(HeaderStruct header)
        {
            Debug.Assert((Command)header.id == Command.Init);
            // read params
            int strategiesNum = _readInt();
            Console.WriteLine("Strategies num: " + strategiesNum);
            if (header.length != 4)
            {
                return false;
            }
            if (onInitEvent != null)
            {
                onInitEvent(strategiesNum);
            }
            return true;
        }

        private bool _handleMap(HeaderStruct header)
        {
            Debug.Assert((Command)header.id == Command.Map);
            // read map info
            int mapSizeX = _readInt();
            int mapSizeY = _readInt();

            Console.WriteLine("Map size: " + mapSizeX + "x" + mapSizeY);

            if (header.length != 4 * 2 + mapSizeX * mapSizeY)
            {
                return false;
            }

            byte[] mapDescr = _read(mapSizeX * mapSizeY);

            try
            {
                if (onMapDescrEvent != null)
                {
                    onMapDescrEvent(new MapDescr(mapSizeX, mapSizeY, mapDescr)); //TODO
                }
            }
            catch (Exception e)
            {                
                String message = "Can not create map. Map data corrupted";
                Console.WriteLine(message);
                Log.error(message);
                MessageBox.Show(message + "\n" + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private bool _handleShips(HeaderStruct header)
        {
            Debug.Assert((Command)header.id == Command.Ships);
            int shipsNum = _readInt();
            ShipInfo[] ships = new ShipInfo[shipsNum];
            
            //Console.Out.WriteLine("Ships Info: " + shipsNum);
            for (int i = 0; i < shipsNum; ++i)
            {
                int id = _readInt();
                int strategyId = _readInt();
                ShipType type = (ShipType)_readInt();
                int speed = _readInt();
                ShipInfo.ShipDirection direction = (ShipInfo.ShipDirection)_readInt();
                int resource = _readInt();
                int posX = _readInt();
                int posY = _readInt();
                int missiles = _readInt();
                int torpedos = _readInt();
                int shells = _readInt();
                int repair = _readInt();
                bool[] visibility = new bool[2]; //TODO
                for (int j = 0; j < 2; ++j)
                {
                    visibility[j] = (_read(1)[0] == 1);
                }
                ships[i] = new ShipInfo(id, strategyId, type, posX, posY, speed, direction, resource, missiles, torpedos, shells, repair, visibility);
                //Console.Out.WriteLine(String.Format("    id:{0}, strategy:{1}, type:{2}, pos:({3},{4}), speed:{5}, direction:{6}, resource:{7}, missiles:{8}, torpedos:{9}, shells:{10}, repair:{11};", id, strategyId, type, posX, posY, speed, direction, resource, missiles, torpedos, shells, repair));
            }

            //TODO: add checks and normal serialize for ship object

            if (onShipsUpdateEvent != null)
            {
                onShipsUpdateEvent(ships);
            }
            return true;
        }

        private bool _handleShots(HeaderStruct header)
        {
            Debug.Assert((Command)header.id == Command.Shots);
            int shotsNum = _readInt();
            ShotInfo[] shots = new ShotInfo[shotsNum];

            //Console.Out.WriteLine("Shots Info: " + shotsNum);

            for (int i = 0; i < shotsNum; ++i)
            {
                int type = _readInt();
                int posX = _readInt();
                int posY = _readInt();
                int destX = _readInt();
                int destY = _readInt();
                int id = _readInt();
                shots[i] = new ShotInfo(type, posX, posY, destX, destY, id);
                //Console.Out.WriteLine(String.Format("    id:{0}, type:{1}, pos:({2},{3}), dest:({4},{5});", id, type, posX, posY, destX, destY));
            }
            //TODO: add checks and normal serialize for ship object
            if (onShotsUpdateEvent != null)
            {
                onShotsUpdateEvent(shots);
            }
            return true;
        }

        private void _process()
        {
            if (_mode == Mode.TCP_MODE && !_openTcpConnection())
            {
                return;
            }

            _changeState(State.Inited);

            try
            {
                HeaderStruct header; // Key - id, Value - len;
                header = _readHeader();
                if ((Command)header.id != Command.Ask || header.length != 0)
                {
                    Console.Out.WriteLine(MSG_INVALID_HEADER);
                    Log.error(MSG_INVALID_HEADER.ToString());
                    _closeConnection();
                    return;
                }
            }
            catch (Exception e)
            {
                String message = "Invalid header";
                Console.WriteLine(message);
                Log.error(message);
                MessageBox.Show(message + "\n" + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _closeConnection();
                return;
            }

            _changeState(State.Established);

            while (!_shutdown && isConnectionEstablished)
            {
                try
                {
                    HeaderStruct header = _readHeader();
                    Command command = (Command)header.id;
                    bool result = false;
                    switch (command)
                    {
                        case Command.Init:
                            {
                                result = _handleInit(header);
                                break;
                            }
                        case Command.Ask:
                            {
                                Debug.Assert(false);
                                break;
                            }
                        case Command.Map:
                            {
                                result = _handleMap(header);
                                break;
                            }
                        case Command.Ships:
                            {
                                result = _handleShips(header);
                                break;
                            }
                        case Command.Shots:
                            {
                                result = _handleShots(header);
                                break;
                            }
                        case Command.EOF:
                            {
                                Console.Out.WriteLine(MSG_END_OF_GAME);
                                Log.info(MSG_END_OF_GAME.ToString());
                                _closeConnection();
                                break;
                            }
                    }
                    if (!result)
                    {
                        _closeConnection();
                    }
                }
                catch (Exception e)
                {
                    String message = "Connection closed";
                    Console.Out.WriteLine(message);
                    Console.Out.WriteLine(e.Message);
                    Log.error(message);
                    Log.error(e.Message);
                    MessageBox.Show(message + "\n" + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _closeConnection();
                }
            }

            _closeConnection();
        }
    }
}
