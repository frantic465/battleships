﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX;

using System.IO;

namespace BattleShips
{
    public class Helpers
    {
        public static Microsoft.DirectX.Vector2 spline(Microsoft.DirectX.Vector2 P1, Microsoft.DirectX.Vector2 T1, Microsoft.DirectX.Vector2 P2, Microsoft.DirectX.Vector2 T2, float s)
        {
            float num = ((2f * ((s * s) * s)) - (3f * (s * s))) + 1f;
            float num2 = (-2f * ((s * s) * s)) + (3f * (s * s));
            float num3 = (((s * s) * s) - (2f * (s * s))) + s;
            float num4 = ((s * s) * s) - (s * s);
            return (Microsoft.DirectX.Vector2)((((num * P1) + (num2 * P2)) + (num3 * T1)) + (num4 * T2));
        }

        public static Vector2 linear(Vector2 a, Vector2 b, float t)
        {
            if (t < 0.0f) t = 0.0f;
            if (t > 1.0f) t = 1.0f;
            return a + (b - a) * t;
        }

        public static float linear(float a, float b, float t)
        {
            if (t < 0.0f) t = 0.0f;
            if (t > 1.0f) t = 1.0f;
            return a + (b - a) * t;
        }

        public static float angleBetweenVectors2(Vector2 v1, Vector2 v2)
        {
            float cross = v1.X * v2.X + v1.Y * v2.Y;
            float sign = Math.Sign(cross);
            cross = Math.Abs(cross);
            float result = (float)Math.Acos(cross / Vector2.Length(v1) / Vector2.Length(v2));
            float pseudoScalar = v1.X * v2.Y - v2.X * v1.Y;
            if (sign < 0)
            {
                result = (float)Math.PI - result;
            }
            if (pseudoScalar > 0)
            {
                result *= -1.0f;
            }
            return result;
        }

        public static Vector3 clipToScreen(Vector3 clip, int screenWidth, int screenHeight)
        {
            return new Vector3((clip.X + 1.0f) / 2.0f * (float) screenWidth, (1.0f - clip.Y) / 2.0f * (float)screenHeight, 1.0f);
        }

        public static Vector3 screenToClip(Vector3 screen, int screenWidth, int screenHeight)
        {
            return new Vector3(screen.X / (float)screenWidth * 2.0f, screen.Y / (float)screenHeight * 2.0f, 1.0f);
        }

        public static void copyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }

        public static T clamp<T>(T value, T min, T max) 
            where T : IComparable<T>
        {
            return value.CompareTo(max) > 0 ? max : (value.CompareTo(min) < 0 ? min : value);
        }
    }
}
