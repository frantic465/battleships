﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BattleShips
{
    public class Log
    {
        public enum OutputLevel
        {
            DEBUG,
            INFO,
            ERROR,
            FATAL,
            NOLOG,
        };

        private static bool _inited = false;
        private static StreamWriter _writer = null;
        public static OutputLevel outputLevel = OutputLevel.DEBUG;

        private static void _init()
        {
            if (!_inited)
            {
                _inited = true;
                _writer = new StreamWriter(File.OpenWrite("log.txt"));
            }
        }

        public static void debug(String message)
        {
            if (outputLevel > OutputLevel.DEBUG)
            {
                return;
            }
            _init();
            _writer.WriteLine("DEBUG\t#:\t" + message);
            _writer.Flush();
        }

        public static void info(String message)
        {
            if (outputLevel > OutputLevel.INFO)
            {
                return;
            }
            _init();
            _writer.WriteLine("INFO\t##:\t" + message);
            _writer.Flush();
        }

        public static void error(String message)
        {
            if (outputLevel > OutputLevel.ERROR)
            {
                return;
            }
            _init();
            _writer.WriteLine("ERROR\t###:\t" + message);
            _writer.Flush();
        }

        public static void fatal(String message)
        {
            if (outputLevel > OutputLevel.FATAL)
            {
                return;
            }
            _init();
            _writer.WriteLine("FATAL\t#####:\t" + message);
            _writer.Flush();
        }
    }
}
