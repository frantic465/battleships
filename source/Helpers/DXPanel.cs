﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BattleShips
{
    class DXPanel : Panel
    {
        public DXPanel() : base() 
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque, true);
        }

        protected override void OnPaint(PaintEventArgs e)
        {            
            base.OnPaint(e);            
        }
    }
}
