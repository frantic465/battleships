namespace BattleShips
{
    using System;
    using System.Reflection;

    public enum ShipType
    {
        BattleShip,
        Cruiser,
        Destroyer,
        MissileCutter,
        RepairBoat
    }
}

