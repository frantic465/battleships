namespace BattleShips
{
    using System;

    [Serializable]
    public struct ShotInfo
    {
        public readonly int destinationX;
        public readonly int destinationY;
        public readonly int id;
        public readonly int type;
        public readonly int posX;
        public readonly int posY;

        public ShotInfo(int type, int x, int y, int destX, int destY, int id)
        {
            this.type = type;
            this.posX = x;
            this.posY = y;
            this.destinationX = destX;
            this.destinationY = destY;
            this.id = id;
        }

        public ShotInfo(ShotInfo shotInfo)
        {
            this.type = shotInfo.type;
            this.posX = shotInfo.posX;
            this.posY = shotInfo.posY;
            this.destinationX = shotInfo.destinationX;
            this.destinationY = shotInfo.destinationY;
            this.id = shotInfo.id;
        }
    }
}

