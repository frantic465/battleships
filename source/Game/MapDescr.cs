﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShips
{
    public class MapDescr
    {
        public enum SurfaceType
        {
            Terrain = 0,
            AlmostTerrain,
            Middle,
            Deep,
            VeryDeep
        };

        private int _sizeX;
        private int _sizeY;
        private SurfaceType[,] _map;

        public MapDescr(int sizeX, int sizeY, byte[] data)
        {
            _sizeX = sizeX;
            _sizeY = sizeY;

            if (data.Length != sizeX * sizeY)
            {
                throw new ArgumentException("Incorrect data size " + data.Length + " != " + sizeX * sizeY);
            }

            _map = new SurfaceType[sizeX, sizeY];

            int num = 0;
            for (int i = 0; i < sizeY; ++i)
            {
                for (int j = 0; j < sizeX; ++j)
                {
                    _map[j, i] = (SurfaceType)data[num++];
                }
            }
        }

        public MapDescr(int sizeX, int sizeY)
        {
            _sizeX = sizeX;
            _sizeY = sizeY;

            _map = new SurfaceType[sizeX, sizeY];

            for (int i = 0; i < sizeY; ++i)
            {
                for (int j = 0; j < sizeX; ++j)
                {
                    _map[j, i] = SurfaceType.VeryDeep;
                }
            }
        }

        public SurfaceType this[int x, int y]
        {
            get { return _map[x, y]; }
            set { _map[x, y] = value; }
        }

        public int sizeX
        {
            get { return _sizeX; }
        }

        public int sizeY
        {
            get { return _sizeY; }
        }
    }
}
