namespace BattleShips
{
    using System;

    [Serializable]
    public struct ShipInfo
    {
        public readonly ShipDirection direction;
        public readonly int id;
        public readonly int missiles;
        public readonly int repair;
        public readonly int resource;
        public readonly int shells;
        public readonly int speed;
        public readonly int strategyId;
        public readonly int torpedoes;
        public readonly ShipType type;
        public readonly bool[] visibility;
        public readonly int posX;
        public readonly int posY;

        public ShipInfo(int id, int strategyId, ShipType type, int x, int y, int speed, ShipDirection direction, int resource, int missiles, int torpedoes, int shells, int repair, bool[] visibility)
        {
            this.id = id;
            this.strategyId = strategyId;
            this.type = type;
            this.posX = x;
            this.posY = y;
            this.speed = speed;
            this.direction = direction;
            this.resource = resource;
            this.missiles = missiles;
            this.torpedoes = torpedoes;
            this.shells = shells;
            this.repair = repair;
            this.visibility = visibility;
        }

        public ShipInfo(ShipInfo shipInfo)
        {
            this.id = shipInfo.id;
            this.strategyId = shipInfo.strategyId;
            this.type = shipInfo.type;
            this.posX = shipInfo.posX;
            this.posY = shipInfo.posY;
            this.speed = shipInfo.speed;
            this.direction = shipInfo.direction;
            this.resource = shipInfo.resource;
            this.missiles = shipInfo.missiles;
            this.torpedoes = shipInfo.torpedoes;
            this.shells = shipInfo.shells;
            this.repair = shipInfo.repair;
            this.visibility = shipInfo.visibility;
        }

        public enum ShipDirection
        {
            North,
            NorthEast,
            East,
            SouthEast,
            South,
            SouthWest,
            West,
            NorthWest
        }
    }
}

