﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace BattleShips
{
    public partial class OpenNetworkConnection : Form
    {
        public String ipAddress = "";
        public int port = 0;


        public OpenNetworkConnection()
        {
            InitializeComponent();
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            ipAddress = ipTextbox.Text;
            port = (int)portTextbox.Value;
            saveConfig();
            Close();
        }

        private void defaultInit()
        {
            ipTextbox.Text = "127.0.0.1";
            portTextbox.Value = 22881;
        }

        private void loadConfig()
        {
            Stream fileStream = null;
            try
            {
                fileStream = File.OpenRead("config");
            }
            catch (Exception)
            {
                Console.Out.WriteLine("Config file not found");
                defaultInit();
                return;
            }

            try
            {
                BinaryReader binaryReader = new BinaryReader(fileStream);
                ipTextbox.Text = binaryReader.ReadString();
                portTextbox.Value = binaryReader.ReadInt32();
                binaryReader.Close();

            }
            catch (Exception)
            {
                Console.Out.WriteLine("Config file is corrupted");
                defaultInit();
                fileStream.Close();
                return;
            }
            fileStream.Close();
        }

        private void saveConfig()
        {
            Stream fileStream = null;
            try
            {
                fileStream = File.OpenWrite("config");
            }
            catch (Exception)
            {
                Console.Out.WriteLine("Can not open config file");
                return;
            }

            try
            {
                BinaryWriter binaryWriter = new BinaryWriter(fileStream);
                binaryWriter.Write(ipTextbox.Text);
                binaryWriter.Write((int)portTextbox.Value);
                binaryWriter.Flush();
                binaryWriter.Close();
            }
            catch (Exception)
            {
                Console.Out.WriteLine("Config file is corrupted");
                defaultInit();
                fileStream.Close();
                return;
            }
            fileStream.Close();
        }

        private void OpenNetworkConnection_Shown(object sender, EventArgs e)
        {
            loadConfig();
        }

        
    }
}
