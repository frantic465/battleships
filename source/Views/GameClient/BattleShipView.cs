﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.DirectX.Direct3D;
using System.Drawing;
using Microsoft.DirectX;

namespace BattleShips
{
    public class BattleShipView : BaseView
    {
        private BattleShipsModel _model = null;

        private TextureWrap _backTexture = null;
        private TextureWrap _mapTexture = null;
        private TextureWrap[,] _shipTexture = new TextureWrap[2, 5];
        private TextureWrap _shotTexture = null;

        private ShipModel _selectedModel = null;

        private bool _insideOnPaint = false;

        private RenderItem _mapRenderItem = null;
        
        public BattleShipView(BattleShipsModel model, Panel renderPanel)
            : base(renderPanel)
        {
            _model = model;
            try
            {
                String applicationPath = Application.StartupPath;
                _backTexture = new TextureWrap(_renderer, 0, applicationPath + "/res/back.jpg");
                _mapTexture = new TextureWrap(_renderer, 1, applicationPath + "/res/tiles.tga");
                _shotTexture = new TextureWrap(_renderer, 3, applicationPath + "/res/shot_0.tga");

                for (int i = 0; i < 2; ++i)
                {
                    for (int j = 0; j < 5; ++j)
                    {
                        String sourceFile = applicationPath + "/res/" + (i + 1) + "_" + (j + 1) + ".tga";
                        int id = 50 + j * 10 + i; // id for sorting and batching
                        _shipTexture[i, j] = new TextureWrap(_renderer, id, sourceFile);
                    }
                }

            }
            catch (Exception)
            {
                String message = "Can not load textures";
                Console.Out.WriteLine(message);
                Log.error(message);
                MessageBox.Show(message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }

        public void release()
        {
            _renderer.release();
        }

        public void assignModel(BattleShipsModel model)
        {
            _model = model;
            _renderer.resetCamera();
        }

        private TextureWrap _getMapTexture()
        {
            return _mapTexture;
        }

        private TextureWrap _getShipTexture(int strategyId, ShipType shipType)
        {
            Debug.Assert(strategyId >= 1 && strategyId <= 2);
            return _shipTexture[strategyId - 1, (int)(shipType - ShipType.BattleShip)];
        }

        private TextureWrap _getShotTexture()
        {
            return _shotTexture;
        }

        override public void pick(float x, float y)
        {
            if (_model == null || _model.mapDescr == null || _model.ships == null)
            {
                return;
            }

            int mapSizeX = _model.mapDescr.sizeX;
            int mapSizeY = _model.mapDescr.sizeY;
            float cellSize = Math.Min(1.0f / mapSizeX, 1.0f / mapSizeY);

            // detect picking radius
            Vector3 radiusInClip = Vector3.TransformCoordinate(new Vector3(cellSize, 0.0f, 0.0f), _renderer.device.Transform.View * _renderer.device.Transform.Projection);
            Vector3 zeroInClip = Vector3.TransformCoordinate(new Vector3(0.0f, 0.0f, 0.0f), _renderer.device.Transform.View * _renderer.device.Transform.Projection);
            Vector3 radiusInScreen = Helpers.clipToScreen(radiusInClip, _renderer.width, _renderer.height);
            Vector3 zeroInScreen = Helpers.clipToScreen(zeroInClip, _renderer.width, _renderer.height);
            float radius = (radiusInScreen - zeroInScreen).Length();

            // pick all ships
            ShipModel selectedModel = null;
            foreach (ShipModel ship in _model.ships)
            {
                ShipModelApproximatedState shipState = ship.getCurrentState();
                if (shipState == null)
                {
                    continue;
                }
                Vector3 gamePosition = new Vector3(shipState.position.X, shipState.position.Y, 0.0f);
                Vector3 worldPosition = new Vector3(shipState.position.X * cellSize, shipState.position.Y * cellSize, 0.0f);
                Vector3 inClipSpace = Vector3.TransformCoordinate(worldPosition, _renderer.device.Transform.View * _renderer.device.Transform.Projection);
                Vector3 inScreen = Helpers.clipToScreen(inClipSpace, _renderer.width, _renderer.height);
                if ((inScreen - new Vector3(x, y, 1.0f)).Length() < radius)
                {
                    selectedModel = ship;
                    break;
                }
            }
            _selectedModel = selectedModel;
            
        }

        private void _defferedInitMapRenderItem()
        {
            if (_mapRenderItem != null)
            {
                return;
            }
            int mapSizeX = _model.mapDescr.sizeX;
            int mapSizeY = _model.mapDescr.sizeY;
            float cellSize = Math.Min(1.0f / mapSizeX, 1.0f / mapSizeY);
            float aspect = (float)mapSizeX / (float)mapSizeY;
            float scaleX = aspect > 1.0f ? 1.0f : (aspect);
            float scaleY = aspect < 1.0f ? 1.0f : (1.0f / aspect);
            float offsetX = (1.0f - scaleX) / 2.0f;
            float offsetY = (1.0f - scaleY) / 2.0f;

            _mapRenderItem = new RenderItem(_getMapTexture(), PrimitiveType.TriangleList, 1);
            for (int i = 0; i < mapSizeX; ++i)
            {
                for (int j = 0; j < mapSizeY; ++j)
                {
                    MapDescr.SurfaceType cellType = (MapDescr.SurfaceType)_model.mapDescr[i, j];
                    if (cellType == MapDescr.SurfaceType.VeryDeep)
                    {
                        continue;
                    }

                    Vector3 leftTop = new Vector3(-cellSize * 0.5f, -cellSize * 0.5f, 0.0f);
                    Vector3 leftBottom = new Vector3(-cellSize * 0.5f, cellSize * 0.5f, 0.0f);
                    Vector3 rightTop = new Vector3(cellSize * 0.5f, -cellSize * 0.5f, 0.0f);
                    Vector3 rightBottom = new Vector3(cellSize * 0.5f, cellSize * 0.5f, 0.0f);

                    Matrix rotation = Matrix.RotationZ(0.0f);
                    Matrix translation = Matrix.Translation((float)i * cellSize, (float)j * cellSize, 0.0f);
                    Matrix transform = rotation * translation;

                    leftTop.TransformCoordinate(transform);
                    leftBottom.TransformCoordinate(transform);
                    rightTop.TransformCoordinate(transform);
                    rightBottom.TransformCoordinate(transform);

                    _mapRenderItem.pushVertex(leftTop.X, leftTop.Y, 0, 0.25f * (float)(cellType), 0);
                    _mapRenderItem.pushVertex(rightTop.X, rightTop.Y, 0, 0.25f * (float)(cellType + 1), 0);
                    _mapRenderItem.pushVertex(rightBottom.X, rightBottom.Y, 0, 0.25f * (float)(cellType + 1), 1);

                    _mapRenderItem.pushVertex(leftTop.X, leftTop.Y, 0, 0.25f * (float)(cellType), 0);
                    _mapRenderItem.pushVertex(rightBottom.X, rightBottom.Y, 0, 0.25f * (float)(cellType + 1), 1);
                    _mapRenderItem.pushVertex(leftBottom.X, leftBottom.Y, 0, 0.25f * (float)(cellType), 1);
                }
            }
        }

        private void _renderInfo()
        {
            // show model buffer usage
            Program.mainWindow.modelProgress.Value = (int)(_model.modelProgress * 100);

            // show info for selected ship
            if (_selectedModel != null && _selectedModel.getCurrentState() != null)
            {
                ShipModelApproximatedState state = _selectedModel.getCurrentState();
                Program.mainWindow.infoPanel.Text = "id\t\t: " + state.info.id + "\n" +
                    "direction\t\t: " + state.info.direction + "\n" +
                    "missiles\t\t: " + state.info.missiles + "\n" +
                    "repair\t\t: " + state.info.repair + "\n" +
                    "resource\t\t: " + state.resources + "\n" +
                    "shells\t\t: " + state.info.shells + "\n" +
                    "speed\t\t: " + state.info.speed + "\n" +
                    "strategy\t\t: " + state.info.strategyId + "\n" +
                    "torpedoes\t\t: " + state.info.torpedoes + "\n" +
                    "type\t\t: " + state.info.type + "\n" +
                    "pos\t\t: " + (int)state.position.X + ", " + (int)state.position.Y + "\n" +
                    "angle\t\t: " + state.angle + "\n";
            }
            else
            {
                Program.mainWindow.infoPanel.Text = "";
            }
        }

        private void _updatePopupLabel()
        {
            if (_model == null)
            {
                Program.mainWindow.popupLabel.Text = "No connection";
                return;
            }

            if (_model.isInGame)
            {
                Program.mainWindow.popupLabel.Text = "In game";
            }
            else
            {
                if (!Program.mainWindow.popupLabel.Text.StartsWith("Strategy"))
                {
                    Program.mainWindow.popupLabel.Text = "No connection";
                }
            }
            

            int aliveStrategiesNum = 0;
            int winner = -1;
            for (int i = 0; i < _model.strategiesNum; ++i)
            {
                if (_model.getShipsByStrategy(i + 1).Count() != 0)
                {
                    ++aliveStrategiesNum;
                    winner = i + 1;
                }
            }

            if (aliveStrategiesNum == 1)
            {
                String message = "Strategy " + winner + " win";
                Program.mainWindow.popupLabel.Text = message;
                Log.info(message);
            }
        }

        private void _renderBack()
        {
            int mapSizeX = _model.mapDescr.sizeX;
            int mapSizeY = _model.mapDescr.sizeY;
            float cellSize = Math.Min(1.0f / mapSizeX, 1.0f / mapSizeY);
            float aspect = (float)mapSizeX / (float)mapSizeY;
            float scaleX = aspect > 1.0f ? 1.0f : (aspect);
            float scaleY = aspect < 1.0f ? 1.0f : (1.0f / aspect);
            float offsetX = (1.0f - scaleX) / 2.0f;
            float offsetY = (1.0f - scaleY) / 2.0f;

            float wcoord = cellSize * mapSizeX;
            float hcoord = cellSize * mapSizeY;
            
            // render back
            RenderItem renderItem = new RenderItem(_backTexture, PrimitiveType.TriangleStrip, 0);
            renderItem.pushVertex(new Vector3(-cellSize / 2.0f, -cellSize / 2.0f, 0), 0, 0);
            renderItem.pushVertex(new Vector3(-cellSize / 2.0f, hcoord - cellSize / 2.0f, 0), 0, hcoord);
            renderItem.pushVertex(new Vector3(wcoord - cellSize / 2.0f, -cellSize / 2.0f, 0), wcoord, 0);
            renderItem.pushVertex(new Vector3(wcoord - cellSize / 2.0f, hcoord - cellSize / 2.0f, 0), wcoord, hcoord);
            _renderer.pushItem(renderItem);
        }

        private void _renderModel()
        {
            int mapSizeX = _model.mapDescr.sizeX;
            int mapSizeY = _model.mapDescr.sizeY;
            float cellSize = Math.Min(1.0f / mapSizeX, 1.0f / mapSizeY);
            float aspect = (float)mapSizeX / (float)mapSizeY;
            float scaleX = aspect > 1.0f ? 1.0f : (aspect);
            float scaleY = aspect < 1.0f ? 1.0f : (1.0f / aspect);
            float offsetX = (1.0f - scaleX) / 2.0f;
            float offsetY = (1.0f - scaleY) / 2.0f;

            // render map

            _defferedInitMapRenderItem();
            _renderer.pushItem(_mapRenderItem);

            // render ships
            if (_model.ships != null)
            {
                foreach (ShipModel ship in _model.ships)
                {
                    ShipModelApproximatedState shipState = ship.getCurrentState();
                    if (shipState == null)
                    {
                        continue;
                    }
                    RenderItem shipItem = new RenderItem(_getShipTexture(shipState.info.strategyId, shipState.info.type), PrimitiveType.TriangleList, 2);
                    Vector3 leftTop = new Vector3(-cellSize * 0.9f, -cellSize * 0.9f, 0.0f);
                    Vector3 leftBottom = new Vector3(-cellSize * 0.9f, cellSize * 0.9f, 0.0f);
                    Vector3 rightTop = new Vector3(cellSize * 0.9f, -cellSize * 0.9f, 0.0f);
                    Vector3 rightBottom = new Vector3(cellSize * 0.9f, cellSize * 0.9f, 0.0f);

                    Matrix rotation = Matrix.RotationZ(shipState.angle);
                    Matrix translation = Matrix.Translation(shipState.position.X * cellSize, shipState.position.Y * cellSize, 0.0f);
                    Matrix transform = rotation * translation;

                    leftTop.TransformCoordinate(transform);
                    leftBottom.TransformCoordinate(transform);
                    rightTop.TransformCoordinate(transform);
                    rightBottom.TransformCoordinate(transform);

                    shipItem.pushVertex(leftTop.X, leftTop.Y, 0, 0, 1);
                    shipItem.pushVertex(rightTop.X, rightTop.Y, 0, 1, 1);
                    shipItem.pushVertex(rightBottom.X, rightBottom.Y, 0, 1, 0);

                    shipItem.pushVertex(leftTop.X, leftTop.Y, 0, 0, 1);
                    shipItem.pushVertex(rightBottom.X, rightBottom.Y, 0, 1, 0);
                    shipItem.pushVertex(leftBottom.X, leftBottom.Y, 0, 0, 0);

                    _renderer.pushItem(shipItem);
                }
            }

            // render shots
            if (_model.shots != null)
            {
                foreach (ShotModel shot in _model.shots)
                {
                    ShotModelApproximatedState shotState = shot.getCurrentState();
                    if (shotState == null)
                    {
                        continue;
                    }
                    RenderItem shotItem = new RenderItem(_getShotTexture(), PrimitiveType.TriangleList, 3);
                    Vector3 leftTop = new Vector3(-cellSize * 0.9f, -cellSize * 0.9f, 0.0f);
                    Vector3 leftBottom = new Vector3(-cellSize * 0.9f, cellSize * 0.9f, 0.0f);
                    Vector3 rightTop = new Vector3(cellSize * 0.9f, -cellSize * 0.9f, 0.0f);
                    Vector3 rightBottom = new Vector3(cellSize * 0.9f, cellSize * 0.9f, 0.0f);

                    Matrix rotation = Matrix.RotationZ(shotState.angle);
                    Matrix translation = Matrix.Translation(shotState.position.X * cellSize, shotState.position.Y * cellSize, 0.0f);
                    Matrix transform = rotation * translation;

                    leftTop.TransformCoordinate(transform);
                    leftBottom.TransformCoordinate(transform);
                    rightTop.TransformCoordinate(transform);
                    rightBottom.TransformCoordinate(transform);

                    shotItem.pushVertex(leftTop.X, leftTop.Y, 0, 0, 1);
                    shotItem.pushVertex(rightTop.X, rightTop.Y, 0, 1, 1);
                    shotItem.pushVertex(rightBottom.X, rightBottom.Y, 0, 1, 0);

                    shotItem.pushVertex(leftTop.X, leftTop.Y, 0, 0, 1);
                    shotItem.pushVertex(rightBottom.X, rightBottom.Y, 0, 1, 0);
                    shotItem.pushVertex(leftBottom.X, leftBottom.Y, 0, 0, 0);

                    _renderer.pushItem(shotItem);
                }
            }
        }

        override public void onPaint()
        {
            if (_insideOnPaint)
            {
                return;
            }
            _insideOnPaint = true;
            _updatePopupLabel();

            if (_renderer == null)
            {
                return;            
            }

            _renderer.clear();
            _renderer.setState();
            _renderer.prepare();

            if (_model != null && _model.mapDescr != null)
            {
                _renderBack();
                _renderInfo();
                _renderModel();
            }

            _renderer.flush();   
            _insideOnPaint = false;
        }        
    }
}
