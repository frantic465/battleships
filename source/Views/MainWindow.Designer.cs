﻿namespace BattleShips
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.menuButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.newConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fromFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.saveReplayButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.connectButton = new System.Windows.Forms.ToolStripButton();
            this.disconnectButton = new System.Windows.Forms.ToolStripButton();
            this.pauseButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.speeddown3 = new System.Windows.Forms.ToolStripButton();
            this.speeddown2 = new System.Windows.Forms.ToolStripButton();
            this.speeddown1 = new System.Windows.Forms.ToolStripButton();
            this.speedup1 = new System.Windows.Forms.ToolStripButton();
            this.speedup2 = new System.Windows.Forms.ToolStripButton();
            this.speedup3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mapEditorButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.infoPanel = new System.Windows.Forms.RichTextBox();
            this.modelProgress = new System.Windows.Forms.ProgressBar();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.newButton = new System.Windows.Forms.ToolStripButton();
            this.saveButton = new System.Windows.Forms.ToolStripButton();
            this.loadButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.instrumentsLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.leaveEditModeButton = new System.Windows.Forms.ToolStripButton();
            this.popupLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuButton,
            this.toolStripSeparator4,
            this.saveReplayButton,
            this.toolStripSeparator3,
            this.connectButton,
            this.disconnectButton,
            this.pauseButton,
            this.toolStripSeparator1,
            this.speeddown3,
            this.speeddown2,
            this.speeddown1,
            this.speedup1,
            this.speedup2,
            this.speedup3,
            this.toolStripSeparator2,
            this.mapEditorButton,
            this.toolStripSeparator5});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(859, 39);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // menuButton
            // 
            this.menuButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.menuButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newConnectionToolStripMenuItem,
            this.fromFileToolStripMenuItem});
            this.menuButton.Image = ((System.Drawing.Image)(resources.GetObject("menuButton.Image")));
            this.menuButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuButton.Name = "menuButton";
            this.menuButton.Size = new System.Drawing.Size(85, 36);
            this.menuButton.Text = "New session";
            // 
            // newConnectionToolStripMenuItem
            // 
            this.newConnectionToolStripMenuItem.Name = "newConnectionToolStripMenuItem";
            this.newConnectionToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.newConnectionToolStripMenuItem.Text = "New connection";
            this.newConnectionToolStripMenuItem.Click += new System.EventHandler(this.newConnectionToolStripMenuItem_Click);
            // 
            // fromFileToolStripMenuItem
            // 
            this.fromFileToolStripMenuItem.Name = "fromFileToolStripMenuItem";
            this.fromFileToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.fromFileToolStripMenuItem.Text = "From file";
            this.fromFileToolStripMenuItem.Click += new System.EventHandler(this.fromFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // saveReplayButton
            // 
            this.saveReplayButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveReplayButton.Image = ((System.Drawing.Image)(resources.GetObject("saveReplayButton.Image")));
            this.saveReplayButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.saveReplayButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveReplayButton.Name = "saveReplayButton";
            this.saveReplayButton.Size = new System.Drawing.Size(36, 36);
            this.saveReplayButton.Text = "Save replay to file";
            this.saveReplayButton.Click += new System.EventHandler(this.saveReplayButton_Click_1);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // connectButton
            // 
            this.connectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.connectButton.Image = ((System.Drawing.Image)(resources.GetObject("connectButton.Image")));
            this.connectButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.connectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(36, 36);
            this.connectButton.Text = "Connect";
            this.connectButton.Click += new System.EventHandler(this.continueButton_Click);
            this.connectButton.Paint += new System.Windows.Forms.PaintEventHandler(this.connectButton_Paint);
            // 
            // disconnectButton
            // 
            this.disconnectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.disconnectButton.Image = ((System.Drawing.Image)(resources.GetObject("disconnectButton.Image")));
            this.disconnectButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.disconnectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(36, 36);
            this.disconnectButton.Text = "Disconnect";
            this.disconnectButton.Click += new System.EventHandler(this.disconnectButton_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pauseButton.Image = ((System.Drawing.Image)(resources.GetObject("pauseButton.Image")));
            this.pauseButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.pauseButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(36, 36);
            this.pauseButton.Text = "Pause";
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // speeddown3
            // 
            this.speeddown3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.speeddown3.Image = ((System.Drawing.Image)(resources.GetObject("speeddown3.Image")));
            this.speeddown3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.speeddown3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.speeddown3.Name = "speeddown3";
            this.speeddown3.Size = new System.Drawing.Size(36, 36);
            this.speeddown3.Text = "Slower";
            this.speeddown3.Click += new System.EventHandler(this.speeddown3_Click);
            // 
            // speeddown2
            // 
            this.speeddown2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.speeddown2.Image = ((System.Drawing.Image)(resources.GetObject("speeddown2.Image")));
            this.speeddown2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.speeddown2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.speeddown2.Name = "speeddown2";
            this.speeddown2.Size = new System.Drawing.Size(28, 36);
            this.speeddown2.Text = "Slower";
            this.speeddown2.Click += new System.EventHandler(this.speeddown2_Click);
            // 
            // speeddown1
            // 
            this.speeddown1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.speeddown1.Image = ((System.Drawing.Image)(resources.GetObject("speeddown1.Image")));
            this.speeddown1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.speeddown1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.speeddown1.Name = "speeddown1";
            this.speeddown1.Size = new System.Drawing.Size(23, 36);
            this.speeddown1.Text = "Slower";
            this.speeddown1.Click += new System.EventHandler(this.speeddown1_Click);
            // 
            // speedup1
            // 
            this.speedup1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.speedup1.Image = ((System.Drawing.Image)(resources.GetObject("speedup1.Image")));
            this.speedup1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.speedup1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.speedup1.Name = "speedup1";
            this.speedup1.Size = new System.Drawing.Size(23, 36);
            this.speedup1.Text = "Faster";
            this.speedup1.Click += new System.EventHandler(this.speedup1_Click);
            // 
            // speedup2
            // 
            this.speedup2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.speedup2.Image = ((System.Drawing.Image)(resources.GetObject("speedup2.Image")));
            this.speedup2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.speedup2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.speedup2.Name = "speedup2";
            this.speedup2.Size = new System.Drawing.Size(28, 36);
            this.speedup2.Text = "Faster";
            this.speedup2.Click += new System.EventHandler(this.speedup2_Click);
            // 
            // speedup3
            // 
            this.speedup3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.speedup3.Image = ((System.Drawing.Image)(resources.GetObject("speedup3.Image")));
            this.speedup3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.speedup3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.speedup3.Name = "speedup3";
            this.speedup3.Size = new System.Drawing.Size(36, 36);
            this.speedup3.Text = "Faster";
            this.speedup3.Click += new System.EventHandler(this.speedup3_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // mapEditorButton
            // 
            this.mapEditorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mapEditorButton.Image = ((System.Drawing.Image)(resources.GetObject("mapEditorButton.Image")));
            this.mapEditorButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.mapEditorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mapEditorButton.Name = "mapEditorButton";
            this.mapEditorButton.Size = new System.Drawing.Size(36, 36);
            this.mapEditorButton.Text = "Map editor";
            this.mapEditorButton.Click += new System.EventHandler(this.mapEditorButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 39);
            // 
            // infoPanel
            // 
            this.infoPanel.BackColor = System.Drawing.SystemColors.Info;
            this.infoPanel.DetectUrls = false;
            this.infoPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.infoPanel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.infoPanel.Location = new System.Drawing.Point(0, 486);
            this.infoPanel.Margin = new System.Windows.Forms.Padding(0);
            this.infoPanel.Name = "infoPanel";
            this.infoPanel.ReadOnly = true;
            this.infoPanel.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.infoPanel.ShortcutsEnabled = false;
            this.infoPanel.Size = new System.Drawing.Size(859, 169);
            this.infoPanel.TabIndex = 2;
            this.infoPanel.TabStop = false;
            this.infoPanel.Text = "";
            // 
            // modelProgress
            // 
            this.modelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.modelProgress.Location = new System.Drawing.Point(0, 476);
            this.modelProgress.Margin = new System.Windows.Forms.Padding(0);
            this.modelProgress.Name = "modelProgress";
            this.modelProgress.Size = new System.Drawing.Size(859, 10);
            this.modelProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.modelProgress.TabIndex = 3;
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(255, 4);
            this.trackBar1.MaximumSize = new System.Drawing.Size(0, 32);
            this.trackBar1.MinimumSize = new System.Drawing.Size(0, 32);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(0, 45);
            this.trackBar1.TabIndex = 4;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newButton,
            this.saveButton,
            this.loadButton,
            this.toolStripSeparator6,
            this.toolStripSeparator7,
            this.instrumentsLabel,
            this.toolStripSeparator8,
            this.leaveEditModeButton});
            this.toolStrip2.Location = new System.Drawing.Point(0, 39);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(859, 39);
            this.toolStrip2.TabIndex = 5;
            this.toolStrip2.Text = "toolStrip2";
            this.toolStrip2.Visible = false;
            // 
            // newButton
            // 
            this.newButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newButton.Image = ((System.Drawing.Image)(resources.GetObject("newButton.Image")));
            this.newButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.newButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(36, 36);
            this.newButton.Text = "New";
            this.newButton.Click += new System.EventHandler(this.newButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveButton.Image = ((System.Drawing.Image)(resources.GetObject("saveButton.Image")));
            this.saveButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.saveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(36, 36);
            this.saveButton.Text = "Save";
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // loadButton
            // 
            this.loadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.loadButton.Image = ((System.Drawing.Image)(resources.GetObject("loadButton.Image")));
            this.loadButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.loadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(36, 36);
            this.loadButton.Text = "Load";
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 39);
            // 
            // instrumentsLabel
            // 
            this.instrumentsLabel.Name = "instrumentsLabel";
            this.instrumentsLabel.Size = new System.Drawing.Size(73, 36);
            this.instrumentsLabel.Text = "Instruments:";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 39);
            // 
            // leaveEditModeButton
            // 
            this.leaveEditModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.leaveEditModeButton.Image = ((System.Drawing.Image)(resources.GetObject("leaveEditModeButton.Image")));
            this.leaveEditModeButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.leaveEditModeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.leaveEditModeButton.Name = "leaveEditModeButton";
            this.leaveEditModeButton.Size = new System.Drawing.Size(36, 36);
            this.leaveEditModeButton.Text = "Leave edit mode";
            this.leaveEditModeButton.Click += new System.EventHandler(this.leaveEditModeButton_Click);
            // 
            // popupLabel
            // 
            this.popupLabel.AutoSize = true;
            this.popupLabel.BackColor = System.Drawing.Color.Maroon;
            this.popupLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.popupLabel.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.popupLabel.ForeColor = System.Drawing.Color.Yellow;
            this.popupLabel.Location = new System.Drawing.Point(4, 44);
            this.popupLabel.Name = "popupLabel";
            this.popupLabel.Size = new System.Drawing.Size(127, 24);
            this.popupLabel.TabIndex = 0;
            this.popupLabel.Text = "Popup message";
            this.popupLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Maroon;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(7, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "label1";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 655);
            this.Controls.Add(this.popupLabel);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.modelProgress);
            this.Controls.Add(this.infoPanel);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BattleShips client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.Shown += new System.EventHandler(this.MainWindow_Shown);
            this.ResizeEnd += new System.EventHandler(this.MainWindow_ResizeEnd);
            this.StyleChanged += new System.EventHandler(this.MainWindow_StyleChanged);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton connectButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton disconnectButton;
        public System.Windows.Forms.RichTextBox infoPanel;
        public System.Windows.Forms.Label popupLabel;
        public System.Windows.Forms.ProgressBar modelProgress;
        private System.Windows.Forms.ToolStripButton pauseButton;
        private System.Windows.Forms.ToolStripDropDownButton menuButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem newConnectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fromFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton saveReplayButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.ToolStripButton speeddown3;
        private System.Windows.Forms.ToolStripButton speeddown2;
        private System.Windows.Forms.ToolStripButton speeddown1;
        private System.Windows.Forms.ToolStripButton speedup1;
        private System.Windows.Forms.ToolStripButton speedup2;
        private System.Windows.Forms.ToolStripButton speedup3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton mapEditorButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton leaveEditModeButton;
        private System.Windows.Forms.ToolStripButton newButton;
        private System.Windows.Forms.ToolStripButton saveButton;
        private System.Windows.Forms.ToolStripButton loadButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripLabel instrumentsLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.Label label1;        
    }
}

