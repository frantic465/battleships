﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.DirectX.Direct3D;
using System.Drawing;
using Microsoft.DirectX;

namespace BattleShips
{
    public class BaseView
    {
        protected Renderer _renderer = null;

        public BaseView(Panel renderPanel)
        {
            _renderer = new Renderer(renderPanel);
        }

        virtual public void onPaint()
        {

        }

        virtual public void onResize()
        {
            _renderer.onResize();            
        }

        virtual public void move(float x, float y)
        {
            _renderer.moveInScreenPixels(x, y);
        }

        virtual public void zoom(float x, float y, float zoom)
        {
            _renderer.zoom(x, y, zoom);
        }

        virtual public void pick(float x, float y)
        {

        }
    }
}
