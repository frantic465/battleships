﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.DirectX;
using System.IO;

namespace BattleShips
{
    public partial class MainWindow : Form
    {
        private const int UPDATE_INTERVAL = 40;
        private float _speed = 1.0f;

        private List<ToolStripButton> _instruments = null;
        private int _selectedInstrument = -1;

        public MainWindow()
        {
            InitializeComponent();

            _initRenderPanel();
            _initEditorInstruments();
        }

        private Timer _timer;
        private Connection _connection = null;
        private BattleShipsModel _model = null;
        private BattleShipView _view = null;
        private ReplayManager _replayManager = null;

        private MapEditorModel _editorModel = null;
        private MapEditorView _editorView = null;

        private BaseView _currentView = null;
        private DXPanel renderPanel = null;

        private void _initRenderPanel()
        {
            renderPanel = new DXPanel();
            renderPanel.Location = new System.Drawing.Point(0, 38);
            renderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            renderPanel.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            renderPanel.Margin = new System.Windows.Forms.Padding(0);
            renderPanel.Name = "renderPanel";
            renderPanel.AutoSize = true;
            renderPanel.Size = new System.Drawing.Size(859, 435);
            renderPanel.TabIndex = 6;
            renderPanel.MouseDown += renderPanel_MouseDown;
            renderPanel.MouseMove += renderPanel_MouseMove;
            renderPanel.MouseUp += renderPanel_MouseUp;
            renderPanel.MouseWheel += renderPanel_MouseWheel;

            this.Controls.Add(renderPanel);
            Control infoPanel = this.Controls["infoPanel"];
            this.Controls.Remove(infoPanel);
            this.Controls.Add(infoPanel);
            
        }

        private void _initEditorInstruments()
        {
            int instrumentsIndex = this.toolStrip2.Items.IndexOfKey("instrumentsLabel") + 1;
            _instruments = new List<ToolStripButton>();
            for (int i = 0; i<5; ++i) 
            {
                ToolStripButton instrument = new ToolStripButton();
                instrument.Checked = false;
                instrument.CheckOnClick = true;
                instrument.CheckState = System.Windows.Forms.CheckState.Unchecked;
                instrument.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
                instrument.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
                instrument.ImageTransparentColor = System.Drawing.Color.Magenta;
                instrument.Image = (System.Drawing.Bitmap)Properties.Resources.ResourceManager.GetObject("instrument" + i, Properties.Resources.Culture);
                instrument.Name = "instrument" + i;
                instrument.Size = new System.Drawing.Size(23, 36);
                instrument.Text = "Instrument " + i;
                instrument.Click += new System.EventHandler(this.instrumentChanged);

                _instruments.Add(instrument);
                this.toolStrip2.Items.Insert(instrumentsIndex, instrument);
                ++instrumentsIndex;
            }
        }

        private void continueButton_Click(object sender, EventArgs e)
        {
            if (_model != null)
            {
                _model.resume();
            }
        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            if (_connection != null)
            {
                _connection.stop();
            }
            _connection = null;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (_currentView != null)
            {
                _currentView.onPaint();            
            } 
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
        }

        private void _onTimer(object sender, EventArgs e)
        {
            if (_model != null)
            {
                _model.update((int)(UPDATE_INTERVAL * _speed));
            }
            if (_currentView != null)
            {
                _currentView.onPaint();
            }

        }

        private void saveReplayToFile()
        {
            if (_replayManager == null)
            {
                String message = "No available replay";
                Log.info(message);
                MessageBox.Show(message, "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Battleships replay|*.bsh";
            saveFileDialog.Title = "Save replay to file";
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName != "")
            {
                Stream fileStream = saveFileDialog.OpenFile();
                _replayManager.saveModelToFile(fileStream);
                fileStream.Close();
            }
        }

        private void connectButton_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void renderPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

        }

        private Point _mouseRightLastPosition;
        private Point _mouseLeftLastPosition;
        private bool _rightMousePressed = false;
        private bool _leftMousePressed = false;
        private bool _mouseMoved = false;

        private void renderPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                _mouseRightLastPosition = e.Location;
                _rightMousePressed = true;
            }
            if (e.Button == MouseButtons.Left)
            {
                _mouseLeftLastPosition = e.Location;
                _leftMousePressed = true;
            }
            _mouseMoved = false;
            renderPanel.Focus();
        }

        private void renderPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (_rightMousePressed)
            {
                _currentView.move((float)(e.X - _mouseRightLastPosition.X), (float)(_mouseRightLastPosition.Y - e.Y));
                _mouseRightLastPosition = e.Location;
            }

            if (_leftMousePressed) 
            {
                if (_currentView == _editorView) //TODO
                {
                    _editorView.pick(e.X, e.Y);
                }
                else
                {
                    _view.move((float)(e.X - _mouseLeftLastPosition.X), (float)(_mouseLeftLastPosition.Y - e.Y));
                    _mouseLeftLastPosition= e.Location;
                }
            }

            _mouseMoved = true;
        }

        private void renderPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                _rightMousePressed = false;
            }

            if (e.Button == MouseButtons.Left)
            {
                _leftMousePressed = false;

                if (_currentView == _view && !_mouseMoved) // TODO
                {
                    _currentView.pick((float)e.X, (float)e.Y);
                }
            }
        }

        private void renderPanel_MouseWheel(object sender, MouseEventArgs e)
        {
            _currentView.zoom(100.0f, 100.0f, (float)e.Delta / 120.0f);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BattleShipsModel.timeBetweenStates += 100;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BattleShipsModel.timeBetweenStates -= 100;
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            if (_model != null)
            {
                _model.pause();
            }
        }

        private void newConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            disconnectButton_Click(sender, e);
            OpenNetworkConnection openNetworkConnection = new OpenNetworkConnection();
            openNetworkConnection.ShowDialog(this);
            
            string hostname = openNetworkConnection.ipAddress;
            int port = openNetworkConnection.port;

            _connection = new Connection(hostname, port);
            _model = new BattleShipsModel(_connection);
            _replayManager = new ReplayManager(_connection);
            _view.assignModel(_model);
            _connection.start();
        }

        private void fromFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            disconnectButton_Click(sender, e);

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Battleships replay|*.bsh";
            openFileDialog.Title = "Load replay";
            openFileDialog.ShowDialog();
            if (openFileDialog.FileName == "")
            {
                return;
            }

            Stream fileStream = openFileDialog.OpenFile();
            _connection = new Connection(fileStream);
            _model = new BattleShipsModel(_connection);
            _replayManager = new ReplayManager(_connection);
            _view.assignModel(_model);
            _connection.start();
        }

        private void saveReplayButton_Click_1(object sender, EventArgs e)
        {
            saveReplayToFile();
        }

        private void speedup3_Click(object sender, EventArgs e)
        {
            _speed += 0.2f;
        }

        private void speedup2_Click(object sender, EventArgs e)
        {
            _speed += 0.1f;
        }

        private void speedup1_Click(object sender, EventArgs e)
        {
            _speed += 0.05f;
        }

        private void speeddown1_Click(object sender, EventArgs e)
        {
            _speed -= 0.05f;
        }

        private void speeddown2_Click(object sender, EventArgs e)
        {
            _speed -= 0.1f;
        }

        private void speeddown3_Click(object sender, EventArgs e)
        {
            _speed -= 0.2f;
        }

        private void mapEditorButton_Click(object sender, EventArgs e)
        {
            _currentView = _editorView;
            toolStrip1.Visible = false;
            toolStrip2.Visible = true;
            pauseButton_Click(sender, e);
        }

        private void leaveEditModeButton_Click(object sender, EventArgs e)
        {
            _currentView = _view;
            toolStrip1.Visible = true;
            toolStrip2.Visible = false;

            _editorModel.reset();
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            SelectMapSizeDialog selectMapSizeDialog = new SelectMapSizeDialog();
            selectMapSizeDialog.ShowDialog(this);

            _editorModel.createNew(selectMapSizeDialog.width, selectMapSizeDialog.height);
            _editorView.forceUpdate();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            _editorModel.saveToFile();
            _editorView.forceUpdate();
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            _editorModel.loadFromFile();
            _editorView.forceUpdate();
        }

        private void instrumentChanged(object sender, EventArgs e)
        {
            _selectedInstrument = -1;
            for (int i = 0; i < _instruments.Count; ++i)
            {
                if (_instruments[i].Equals(sender)) {
                    _selectedInstrument = i;
                    _instruments[i].Checked = true;
                } 
                else 
                {
                    _instruments[i].Checked = false;
                }
            }
            _editorModel.selectInstrument(_selectedInstrument);
        }

        private void MainWindow_Shown(object sender, EventArgs e)
        {
            base.MaximizeBox = false;

            _view = new BattleShipView(null, renderPanel);
            _currentView = _view;

            _editorModel = new MapEditorModel();
            _editorView = new MapEditorView(_editorModel, renderPanel);

            _timer = new System.Windows.Forms.Timer();
            _timer.Interval = UPDATE_INTERVAL;
            _timer.Tick += new EventHandler(_onTimer);
            _timer.Start();       
        }

        private void MainWindow_ResizeEnd(object sender, EventArgs e)
        {
            if (_currentView != null)
            {
                _currentView.onResize();
                _currentView.onPaint();
            }            
        }

        
        private void MainWindow_StyleChanged(object sender, EventArgs e)
        {

        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            _view.release();
            _editorView.release();
        }
    }
}
