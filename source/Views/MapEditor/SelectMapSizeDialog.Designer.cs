﻿namespace BattleShips
{
    partial class SelectMapSizeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.widthLabel = new System.Windows.Forms.Label();
            this.widthTextbox = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.heightLabel = new System.Windows.Forms.Label();
            this.heightTextbox = new System.Windows.Forms.NumericUpDown();
            this.createButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.widthTextbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightTextbox)).BeginInit();
            this.SuspendLayout();
            // 
            // widthLabel
            // 
            this.widthLabel.AutoSize = true;
            this.widthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.widthLabel.Location = new System.Drawing.Point(12, 9);
            this.widthLabel.Name = "widthLabel";
            this.widthLabel.Size = new System.Drawing.Size(42, 16);
            this.widthLabel.TabIndex = 0;
            this.widthLabel.Text = "Width";
            // 
            // widthTextbox
            // 
            this.widthTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.widthTextbox.Location = new System.Drawing.Point(66, 7);
            this.widthTextbox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.widthTextbox.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.widthTextbox.Name = "widthTextbox";
            this.widthTextbox.Size = new System.Drawing.Size(120, 22);
            this.widthTextbox.TabIndex = 1;
            this.widthTextbox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Width";
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.heightLabel.Location = new System.Drawing.Point(12, 35);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(47, 16);
            this.heightLabel.TabIndex = 0;
            this.heightLabel.Text = "Height";
            // 
            // heightTextbox
            // 
            this.heightTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.heightTextbox.Location = new System.Drawing.Point(66, 33);
            this.heightTextbox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.heightTextbox.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.heightTextbox.Name = "heightTextbox";
            this.heightTextbox.Size = new System.Drawing.Size(120, 22);
            this.heightTextbox.TabIndex = 1;
            this.heightTextbox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // createButton
            // 
            this.createButton.Location = new System.Drawing.Point(66, 61);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(120, 23);
            this.createButton.TabIndex = 2;
            this.createButton.Text = "Create";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler(this.createButton_Click);
            // 
            // SelectMapSizeDialog
            // 
            this.AcceptButton = this.createButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(202, 90);
            this.ControlBox = false;
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.heightTextbox);
            this.Controls.Add(this.heightLabel);
            this.Controls.Add(this.widthTextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.widthLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SelectMapSizeDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select map size";
            ((System.ComponentModel.ISupportInitialize)(this.widthTextbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightTextbox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label widthLabel;
        private System.Windows.Forms.NumericUpDown widthTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.NumericUpDown heightTextbox;
        private System.Windows.Forms.Button createButton;
    }
}