﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BattleShips
{
    public partial class SelectMapSizeDialog : Form
    {
        public SelectMapSizeDialog()
        {
            InitializeComponent();
        }

        public int width;
        public int height;

        private void createButton_Click(object sender, EventArgs e)
        {
            width = (int)widthTextbox.Value;
            height = (int)heightTextbox.Value;
            Close();
        }
    }
}
