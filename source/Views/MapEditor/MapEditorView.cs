﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.Diagnostics;
using Microsoft.DirectX.Direct3D;
using System.Drawing;
using Microsoft.DirectX;

namespace BattleShips
{
    public class MapEditorView : BaseView
    {
        private MapEditorModel _model = null;

        private TextureWrap _backTexture = null;
        private TextureWrap _mapTexture = null;
        private RenderItem _mapRenderItem = null;
        private bool _needUpdate = true;

        private bool _insideOnPaint;

        public MapEditorView(MapEditorModel model, Panel renderPanel)
            :base(renderPanel)
        {
            _model = model;

            String applicationPath = Application.StartupPath;

            try
            {
                _backTexture = new TextureWrap(_renderer, 0, applicationPath + "/res/back.jpg");
                _mapTexture = new TextureWrap(_renderer, 1, applicationPath + "/res/tiles.tga");
            }
            catch (Exception)
            {
                String message = "Can not load textures";
                Console.Out.WriteLine(message);
                Log.error(message);
                MessageBox.Show(message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }            
        }

        public void release()
        {
            _renderer.release();
        }

        public void assignModel(MapEditorModel model)
        {
            _model = model;
            _renderer.resetCamera();
        }

        private Vector3 worldToScreen(Vector3 worldPosition)
        {            
            Vector3 inClipSpace = Vector3.TransformCoordinate(worldPosition, _renderer.device.Transform.View * _renderer.device.Transform.Projection);
            Vector3 inScreen = Helpers.clipToScreen(inClipSpace, _renderer.width, _renderer.height);
            return inScreen;
        }

        public void forceUpdate()
        {
            _needUpdate = true;
        }

        override public void pick(float x, float y)
        {
            if (_model == null || _model.mapDescr == null)
            {
                return;
            }

            int mapSizeX = _model.mapDescr.sizeX;
            int mapSizeY = _model.mapDescr.sizeY;
            float cellSize = Math.Min(1.0f / mapSizeX, 1.0f / mapSizeY);
            
            float wcoord = cellSize * mapSizeX;
            float hcoord = cellSize * mapSizeY;

            // detect picking radius
            Vector3 radiusInClip = Vector3.TransformCoordinate(new Vector3(cellSize, 0.0f, 0.0f), _renderer.device.Transform.View * _renderer.device.Transform.Projection);
            Vector3 zeroInClip = Vector3.TransformCoordinate(new Vector3(0.0f, 0.0f, 0.0f), _renderer.device.Transform.View * _renderer.device.Transform.Projection);
            Vector3 radiusInScreen = Helpers.clipToScreen(radiusInClip, _renderer.width, _renderer.height);
            Vector3 zeroInScreen = Helpers.clipToScreen(zeroInClip, _renderer.width, _renderer.height);
            float radius = (radiusInScreen - zeroInScreen).Length();

            Vector3 leftBottomScreen = worldToScreen(new Vector3(0.0f, 0.0f, 0.0f));
            Vector3 rightTopScreen = worldToScreen(new Vector3(wcoord - cellSize, hcoord - cellSize, 0.0f));

            int gameXCoord = (int)((x - leftBottomScreen.X) / (rightTopScreen.X - leftBottomScreen.X) * mapSizeX);
            int gameYCoord = (int)((y - leftBottomScreen.Y) / (rightTopScreen.Y - leftBottomScreen.Y) * mapSizeY);

            // clamping
            gameXCoord = Helpers.clamp(gameXCoord, 0, mapSizeX - 1);
            gameYCoord = Helpers.clamp(gameYCoord, 0, mapSizeY - 1);

            if (_model.currentInstrument >= (int)MapDescr.SurfaceType.Terrain && _model.currentInstrument <= (int)MapDescr.SurfaceType.VeryDeep)
            {
                _model.mapDescr[gameXCoord, gameYCoord] = (MapDescr.SurfaceType)_model.currentInstrument;
            }

            _needUpdate = true;
        }

        private void _defferedInitMapRenderItem()
        {
            if (_mapRenderItem != null && !_needUpdate)
            {
                return;
            }
            _needUpdate = false;

            int mapSizeX = _model.mapDescr.sizeX;
            int mapSizeY = _model.mapDescr.sizeY;
            float cellSize = Math.Min(1.0f / mapSizeX, 1.0f / mapSizeY);
            float aspect = (float)mapSizeX / (float)mapSizeY;
            float scaleX = aspect > 1.0f ? 1.0f : (aspect);
            float scaleY = aspect < 1.0f ? 1.0f : (1.0f / aspect);
            float offsetX = (1.0f - scaleX) / 2.0f;
            float offsetY = (1.0f - scaleY) / 2.0f;

            _mapRenderItem = new RenderItem(_mapTexture, PrimitiveType.TriangleList, 1);
            for (int i = 0; i < mapSizeX; ++i)
            {
                for (int j = 0; j < mapSizeY; ++j)
                {
                    MapDescr.SurfaceType cellType = (MapDescr.SurfaceType)_model.mapDescr[i, j];
                    if (cellType == MapDescr.SurfaceType.VeryDeep)
                    {
                        continue;
                    }

                    Vector3 leftTop = new Vector3(-cellSize * 0.5f, -cellSize * 0.5f, 0.0f);
                    Vector3 leftBottom = new Vector3(-cellSize * 0.5f, cellSize * 0.5f, 0.0f);
                    Vector3 rightTop = new Vector3(cellSize * 0.5f, -cellSize * 0.5f, 0.0f);
                    Vector3 rightBottom = new Vector3(cellSize * 0.5f, cellSize * 0.5f, 0.0f);

                    Matrix rotation = Matrix.RotationZ(0.0f);
                    Matrix translation = Matrix.Translation((float)i * cellSize, (float)j * cellSize, 0.0f);
                    Matrix transform = rotation * translation;

                    leftTop.TransformCoordinate(transform);
                    leftBottom.TransformCoordinate(transform);
                    rightTop.TransformCoordinate(transform);
                    rightBottom.TransformCoordinate(transform);

                    _mapRenderItem.pushVertex(leftTop.X, leftTop.Y, 0, 0.25f * (float)(cellType), 0);
                    _mapRenderItem.pushVertex(rightTop.X, rightTop.Y, 0, 0.25f * (float)(cellType + 1), 0);
                    _mapRenderItem.pushVertex(rightBottom.X, rightBottom.Y, 0, 0.25f * (float)(cellType + 1), 1);

                    _mapRenderItem.pushVertex(leftTop.X, leftTop.Y, 0, 0.25f * (float)(cellType), 0);
                    _mapRenderItem.pushVertex(rightBottom.X, rightBottom.Y, 0, 0.25f * (float)(cellType + 1), 1);
                    _mapRenderItem.pushVertex(leftBottom.X, leftBottom.Y, 0, 0.25f * (float)(cellType), 1);
                }
            }
        }

        private void _renderBack()
        {
            int mapSizeX = _model.width;
            int mapSizeY = _model.height;
            float cellSize = Math.Min(1.0f / mapSizeX, 1.0f / mapSizeY);
            float aspect = (float)mapSizeX / (float)mapSizeY;
            float scaleX = aspect > 1.0f ? 1.0f : (aspect);
            float scaleY = aspect < 1.0f ? 1.0f : (1.0f / aspect);
            float offsetX = (1.0f - scaleX) / 2.0f;
            float offsetY = (1.0f - scaleY) / 2.0f;

            float wcoord = cellSize * mapSizeX;
            float hcoord = cellSize * mapSizeY;

            // render back
            RenderItem renderItem = new RenderItem(_backTexture, PrimitiveType.TriangleStrip, 0);
            renderItem.pushVertex(new Vector3(-cellSize / 2.0f, -cellSize / 2.0f, 0), 0, 0);
            renderItem.pushVertex(new Vector3(-cellSize / 2.0f, hcoord - cellSize / 2.0f, 0), 0, hcoord);
            renderItem.pushVertex(new Vector3(wcoord - cellSize / 2.0f, -cellSize / 2.0f, 0), wcoord, 0);
            renderItem.pushVertex(new Vector3(wcoord - cellSize / 2.0f, hcoord - cellSize / 2.0f, 0), wcoord, hcoord);
            _renderer.pushItem(renderItem);
        }

        private void _updatePopupLabel()
        {
            if (!Program.mainWindow.popupLabel.Text.Equals("Editor"))
            {
                Program.mainWindow.popupLabel.Text = "Editor";
            }            
        }

        private void _renderModel()
        {
            int mapSizeX = _model.mapDescr.sizeX;
            int mapSizeY = _model.mapDescr.sizeY;
            float cellSize = Math.Min(1.0f / mapSizeX, 1.0f / mapSizeY);
            float aspect = (float)mapSizeX / (float)mapSizeY;
            float scaleX = aspect > 1.0f ? 1.0f : (aspect);
            float scaleY = aspect < 1.0f ? 1.0f : (1.0f / aspect);
            float offsetX = (1.0f - scaleX) / 2.0f;
            float offsetY = (1.0f - scaleY) / 2.0f;

            // render map

            _defferedInitMapRenderItem();
            _renderer.pushItem(_mapRenderItem);

        }

        override public void onPaint()
        {
            if (_insideOnPaint)
            {
                return;
            }
            _insideOnPaint = true;
            _updatePopupLabel();

            if (_renderer == null)
            {
                return;
            }

            _renderer.clear();
            _renderer.setState();
            _renderer.prepare();

            if (_model != null && _model.mapDescr != null)
            {
                _renderBack();
                _renderModel();
            }

            _renderer.flush();
            _insideOnPaint = false;
        }
    }
}
