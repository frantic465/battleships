﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShips
{
    public class RenderItemHelper
    {
        public static int getHash(RenderItem renderItem)
        {
            int result = 3;
            result += (int)renderItem.order * 100000;
            result += (int)renderItem.primitiveType * 1000;
            result += renderItem.texture.id;
            return result;
        }
    }
}
