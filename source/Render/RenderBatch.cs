﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.DirectX.Direct3D;

namespace BattleShips
{
    public class RenderBatch
    {
        private List<RenderItem> _renderItems;
        private TextureWrap _texture;
        private PrimitiveType _primitiveType;

        public RenderBatch(TextureWrap texture, PrimitiveType primitiveType)
        {
            _texture = texture;
            _primitiveType = primitiveType;
            _renderItems = new List<RenderItem>();
        }

        public TextureWrap texture
        {
            get { return _texture; }
        }

        public PrimitiveType primitiveType
        {
            get { return _primitiveType; }
        }

        public IEnumerable<RenderItem> renderItems
        {
            get { return _renderItems; }
        }

        public int verticesCount
        {
            get
            {
                int result = 0;
                foreach (RenderItem renderItem in _renderItems)
                {
                    result += renderItem.verticesCount;
                }
                return result;
            }
        }

        public int primitiveCount
        {
            get
            {
                int result = 0;
                foreach (RenderItem renderItem in _renderItems)
                {
                    result += renderItem.primitiveCount;
                }
                return result;
            }
        }

        public void push(RenderItem renderItem)
        {
            Debug.Assert(renderItem.texture.id == _texture.id);
            _renderItems.Add(renderItem);            
        }
    }
}
