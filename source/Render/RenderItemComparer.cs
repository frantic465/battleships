﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShips
{
    public class RenderItemComparer : IComparer<RenderItem>
    {
        public int Compare(RenderItem a, RenderItem b)
        {
            if (a == null && b == null)
            {
                if (a.primitiveType == b.primitiveType)
                {
                    return 0;
                }
                if (a.primitiveType > b.primitiveType)
                {
                    return 1;
                }
                return -1;
            }
            if (a == null)
            {
                return -1;
            }
            if (b == null)
            {
                return 1;
            }
            if (a.texture.id == b.texture.id)
            {
                if (a.primitiveType == b.primitiveType)
                {
                    return 0;
                }
                if (a.primitiveType > b.primitiveType)
                {
                    return 1;
                }
                return -1;

            }
            if (a.texture.id > b.texture.id)
            {
                return 1;
            }
            return -1;
        }
    }

    public class RenderItemHashComparer : IComparer<RenderItem>
    {
        public int Compare(RenderItem a, RenderItem b)
        {
            if (a.hash == b.hash)
            {
                return 0;
            }
            else if (a.hash > b.hash)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }
    }
}
