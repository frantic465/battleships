﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using Microsoft;
using System.Threading;

namespace BattleShips
{
    public class Renderer
    {
        private Control _window = null;
        private List<RenderItem> _renderCollection = new List<RenderItem>();
        private Device _device = null;
        private const float FOV = (float)Math.PI / 4.0f;
        private bool _isDeviceLost = false;
        private Matrix _view;

        public delegate void UnloadResourcesHandler();
        public event UnloadResourcesHandler onUnloadResourcesEvent;

        public delegate void ReloadResourcesHandler();
        public event ReloadResourcesHandler onReloadResourcesEvent;

        public Renderer(Control control)
        {         
            _window = control;
            _view = Matrix.Translation(-0.5f, -0.5f, 1.5f);
            _initDirect3D();
        }

        public Device device
        {
            get { return _device; }
        }

        private PresentParameters _getPresentationParams()
        {
            PresentParameters presentParameters = new PresentParameters();
            presentParameters.SwapEffect = SwapEffect.Discard;
            presentParameters.Windowed = true;
            presentParameters.EnableAutoDepthStencil = true;
            presentParameters.AutoDepthStencilFormat = DepthFormat.D16;
            presentParameters.BackBufferCount = 1;
            presentParameters.BackBufferFormat = Format.A8R8G8B8;
            presentParameters.BackBufferWidth = _window.Width;
            presentParameters.BackBufferHeight = _window.Height;
            presentParameters.DeviceWindow = _window;
            presentParameters.DeviceWindowHandle = _window.Handle;


            return presentParameters;
        }

        private  void _initDirect3D()
        {
            PresentParameters presentParameters = _getPresentationParams();

            _device = new Device(0, DeviceType.Hardware, _window.Handle, CreateFlags.SoftwareVertexProcessing, presentParameters);
            _device.DeviceReset += new EventHandler(_onDeviceReset);
            _device.DeviceResizing += new CancelEventHandler(_onDeviceResizing);
            _device.DeviceLost += new EventHandler(_onDeviceLost);

            VertexElement[] vertexElements = new VertexElement[]
            {
                new VertexElement(0, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0),
                new VertexElement(1, 0, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TextureCoordinate, 0),
                VertexElement.VertexDeclarationEnd
            };
            _device.VertexDeclaration = new VertexDeclaration(_device, vertexElements);

            float aspect = (float)_window.Width / (float)_window.Height;
            _device.Transform.Projection = Matrix.PerspectiveFovLH(FOV, aspect, 0.1f, 100.0f);
            _device.Transform.World = Matrix.Translation(0.0f, 0.0f, 0.0f);
            _device.Transform.View = _view;
        }

        public void release()
        {
            if (onUnloadResourcesEvent != null)
            {
                onUnloadResourcesEvent();
            }

            _device.Dispose();
            _device = null;
        }

        public void onResize()
        {
            if (onUnloadResourcesEvent != null)
            {
                onUnloadResourcesEvent();
            }

            _initDirect3D();

            if (onReloadResourcesEvent != null)
            {
                onReloadResourcesEvent();
            }
        }

        private void _onDeviceReset(object sender, EventArgs e)
        {
            
        }

        private void _onDeviceLost(object sender, EventArgs e)
        {   
        }

        private void _onDeviceResizing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

        public void updateProjection()
        {
            
        }

        public void setState()
        {
            _device.RenderState.AlphaBlendEnable = true;
            _device.RenderState.AlphaBlendOperation = BlendOperation.Add;
            _device.RenderState.SourceBlend = Blend.SourceAlpha;
            _device.RenderState.DestinationBlend = Blend.InvSourceAlpha;
            _device.RenderState.AlphaTestEnable = true;
            _device.RenderState.ReferenceAlpha = 0;
            _device.RenderState.AlphaFunction = Compare.Greater;

            _device.RenderState.CullMode = Cull.None;
            _device.RenderState.Lighting = false;
            _device.RenderState.ZBufferEnable = true;

            _device.SamplerState[0].MinFilter = TextureFilter.Linear;
            _device.SamplerState[0].MagFilter = TextureFilter.Linear;
            _device.SamplerState[0].MipFilter = TextureFilter.Linear;
            _device.SamplerState[1].MinFilter = TextureFilter.Linear;
            _device.SamplerState[1].MagFilter = TextureFilter.Linear;
            _device.SamplerState[1].MipFilter = TextureFilter.Linear;
        }


        public void prepare()
        {
            _renderCollection.Clear();
        }

        public void pushItem(RenderItem renderItem)
        {
            _renderCollection.Add(renderItem);
            
        }

        public Vector3 getCameraPosition()
        {
            Matrix view = _device.Transform.View;
            return new Vector3(view.M41, view.M42, view.M43);
        }

        public int width
        {
            get { return _window.Width; }
        }

        public int height
        {
            get { return _window.Height; }
        }

        public void moveInScreenPixels(float x, float y)
        {
            // 
            Matrix projection = _device.Transform.Projection;
            Matrix invProjection = Matrix.Invert(projection);
            Matrix world = _device.Transform.World;            
            Matrix view = _device.Transform.View;
            Matrix invView = Matrix.Invert(_device.Transform.View);
            
            // transform screen to clip space
            Vector3 screen = new Vector3(x, y, 1.0f);
            Vector3 clip = Helpers.screenToClip(screen, _window.Width, _window.Height);

            Vector3 cameraPos = getCameraPosition();
            _view = _view * Matrix.Translation(clip.X * cameraPos.Z, clip.Y * cameraPos.Z, 0.0f);
            _device.Transform.View = _view;
            
        }

        public void zoom(float x, float y, float zoom)
        {
            float scale = 0.1f * Math.Abs(getCameraPosition().Z - 0.1f);
            if (scale > 2.0f)
            {
                scale = 2.0f;
            }
            _view = _view * Matrix.Translation(0.0f, 0.0f, zoom * scale);
            _device.Transform.View = _view;
        }

        public void resetCamera()
        {
            _view = Matrix.Translation(-0.5f, -0.5f, 1.5f);
            _device.Transform.View = _view;
        }

        private void _flushRenderBatch(RenderBatch renderBatch)
        {
            _device.SetTexture(0, renderBatch.texture.hardwareTexture);
            VertexBuffer vertexBuffer = new VertexBuffer(typeof(RenderItem.Position), renderBatch.verticesCount, _device, Usage.Dynamic, VertexFormats.Position, Pool.Default);
            VertexBuffer texCoordBuffer = new VertexBuffer(typeof(RenderItem.TexCoord), renderBatch.verticesCount, _device, Usage.Dynamic, VertexFormats.Texture1, Pool.Default);
            GraphicsStream vertexStream = vertexBuffer.Lock(0, 0, LockFlags.None);
            GraphicsStream texCoordsStream = texCoordBuffer.Lock(0, 0, LockFlags.None);
            foreach (RenderItem item in renderBatch.renderItems)
            {
                vertexStream.Write(item.positions);
                texCoordsStream.Write(item.texCoords);
            }
            vertexBuffer.Unlock();
            texCoordBuffer.Unlock();
            
            _device.SetStreamSource(0, vertexBuffer, 0);
            _device.SetStreamSource(1, texCoordBuffer, 0);
            _device.DrawPrimitives(renderBatch.primitiveType, 0, renderBatch.primitiveCount);

            vertexBuffer.Dispose();
            texCoordBuffer.Dispose();
        }

        public void clear()
        {
            _device.Clear(ClearFlags.Target | ClearFlags.ZBuffer, System.Drawing.Color.Gray, 1.0f, 0);
        }

        public void flush()
        {
            if (_isDeviceLost)
            {
                Thread.Sleep(300);
                try
                {
                    _device.Present();
                    _isDeviceLost = false;
                }
                catch (DeviceLostException)
                {
                    return;
                }
                catch (DeviceNotResetException)
                {
                    try
                    {
                        PresentParameters presentationParams = _getPresentationParams();
                        _device.Reset(presentationParams);
                        float aspect = (float)_window.Width / (float)_window.Height;
                        _device.Transform.Projection = Matrix.PerspectiveFovLH(FOV, aspect, 0.1f, 100.0f);
                        _isDeviceLost = false;
                        return;
                    }
                    catch (Exception)
                    {
                        return;
                    }
                }
            }

            try
            {
                _device.BeginScene();

                _renderCollection.Sort(new RenderItemHashComparer());

                RenderBatch renderBatch = null;

                foreach (RenderItem renderItem in _renderCollection)
                {
                    if (renderItem.verticesCount == 0)
                    {
                        continue;
                    }

                    if (renderBatch != null && (renderBatch.texture.id != renderItem.texture.id || renderBatch.primitiveType != renderItem.primitiveType))
                    {
                        // render batch with current batching texture
                        _flushRenderBatch(renderBatch);
                        renderBatch = null;
                    }

                    if (renderBatch == null)
                    {
                        renderBatch = new RenderBatch(renderItem.texture, renderItem.primitiveType);
                    }
                    renderBatch.push(renderItem);
                }

                if (renderBatch != null)
                {
                    _flushRenderBatch(renderBatch);
                    renderBatch = null;
                }


                _device.EndScene();
                _device.Present();
            }
            catch (DeviceLostException e)
            {
                _isDeviceLost = true;
                MessageBox.Show("Device lost " + e.Message);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.StackTrace);
                Application.Exit();
            }
        }
    }
}