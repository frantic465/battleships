﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX;

namespace BattleShips
{
    public class RenderItem
    {
        public struct Position
        {
            float x, y, z;
            public Position(float x, float y, float z)
            {
                this.x = x;
                this.y = y;
                this.z = z;
            }
        };

        public struct TexCoord
        {
            float tu, tv;

            public TexCoord(float tu, float tv)
            {
                this.tu = tu;
                this.tv = tv;
            }

        };

        private List<Position> _positions = null;
        private Position[] _positionsArray = null;
        private List<TexCoord> _texCoords = null;
        private TexCoord[] _texCoordsArray = null;
        private bool _needToUpdatePositionsArray = true;
        private bool _needToUpdateTexCoordsArray = true;
        
        public readonly int hash;
        public readonly int order;
        public readonly TextureWrap texture;
        public readonly PrimitiveType primitiveType;

        public RenderItem(TextureWrap texture, PrimitiveType primitiveType, int order)
        {
            this.texture = texture;
            this.primitiveType = primitiveType;
            hash = RenderItemHelper.getHash(this);
            this.order = order;
            
            _positions = new List<Position>();
            _texCoords = new List<TexCoord>();            
        }

        public void pushVertex(float x, float y, float z, float tu, float tv)
        {
            _positions.Add(new Position(x, y, z));
            _texCoords.Add(new TexCoord(tu, tv));

            _needToUpdatePositionsArray = true;
            _needToUpdateTexCoordsArray = true;
        }

        public void pushVertex(Vector3 pos, float tu, float tv)
        {
            _positions.Add(new Position(pos.X, pos.Y, pos.Z));
            _texCoords.Add(new TexCoord(tu, tv));

            _needToUpdatePositionsArray = true;
            _needToUpdateTexCoordsArray = true;
        }

        public Position[] positions
        {
            get
            {
                if (_needToUpdatePositionsArray)
                {
                    _needToUpdatePositionsArray = false;
                    _positionsArray = _positions.ToArray();
                }
                return _positionsArray;
            }
        }

        public TexCoord[] texCoords
        {
            get 
            {
                if (_needToUpdateTexCoordsArray)
                {
                    _needToUpdateTexCoordsArray = false;
                    _texCoordsArray = _texCoords.ToArray();
                }
                return _texCoordsArray;
            }
        }

        public int verticesCount
        {
            get { return _positions.Count; }
        }

        public int primitiveCount
        {
            get
            {
                switch (primitiveType)
                {
                    case PrimitiveType.TriangleStrip:
                        {
                            return Math.Max(0, _positions.Count - 2); 
                        }
                    case PrimitiveType.LineList:                        
                    case PrimitiveType.LineStrip:
                    case PrimitiveType.PointList:
                    case PrimitiveType.TriangleFan:
                        {
                            Debug.Assert(false);
                            return 0;
                        }
                    case PrimitiveType.TriangleList:                        
                        {
                            return _positions.Count / 3;
                        }
                }
                Debug.Assert(false);
                return 0;
            }
        }
    }
}
