﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.DirectX.Direct3D;

namespace BattleShips
{
    public class TextureWrap
    {
        public Texture hardwareTexture;
        public int id;
        public String fileName;
        public Renderer renderer;

        public TextureWrap(Renderer renderer, int id, String fileName)
        {
            this.id = id;
            this.hardwareTexture = TextureLoader.FromFile(renderer.device, fileName, 0, 0, 0, Usage.None, Format.A8R8G8B8, Pool.Managed, Filter.Box, Filter.Box, 0x00000000);
            this.renderer = renderer;
            this.fileName = fileName;
            this.renderer.onUnloadResourcesEvent += onUnloadHandler;
            this.renderer.onReloadResourcesEvent += onReloadHandler;
        }

        private void onUnloadHandler()
        {
            hardwareTexture.Dispose();
            hardwareTexture = null;
        }

        private void onReloadHandler()
        {
            hardwareTexture = TextureLoader.FromFile(renderer.device, fileName, 0, 0, 0, Usage.None, Format.A8R8G8B8, Pool.Managed, Filter.Box, Filter.Box, 0x00000000);
        }
    }
}
